<?php
// Ignore this if-statement, it serves only to prevent running this file directly.
if (!class_exists(Aerys\Process::class, false)) {
    echo "This file is not supposed to be invoked directly. To run it, use `php bin/aerys -c demo.php`.\n";
    exit(1);
}
use Aerys\{ Host, Request, Response, function root, function router};
/* --- Global server options -------------------------------------------------------------------- */
const AERYS_OPTIONS = [
    "connectionTimeout" => 60,
    //"deflateMinimumLength" => 0,
    "sendServerToken" => true,
];

// Same set of parameters, passed using an URI string:
$predis = new Predis\Client('tcp://127.0.0.1:6379');

/* --- http://localhost:1337/ ------------------------------------------------------------------- */
$router = router()
    ->route("GET", "/", function(Request $req, Response $res) {
        $res->end("<html><body><h1>Hello, world.</h1></body></html>");
    })
    ->route("GET", "/test/", function(Request $req, Response $res, array $routeArgs) use ($predis) {
        $inc = $predis->incr('test');
        $res->end($inc);
    });
// If none of our routes match try to serve a static file
$root = root($docrootPath = __DIR__);
// If no static files match fallback to this
$fallback = function(Request $req, Response $res) {
    $res->end("<html><body><h1>Fallback \o/</h1></body></html>");
};
return (new Host)->expose("*", 1337)->use($router)->use($root)->use($fallback);